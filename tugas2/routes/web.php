<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// routing ganjilgenap
Route::get('/ganjilgenap', 'GanjilGenapController@index');
Route::post('/ganjilgenap', 'GanjilGenapController@setProses')->name('setproses');

// routing hitungvocal
Route::get('/hitungvocal', 'HitungVocalController@index');
Route::post('/hitungvocal', 'HitungVocalController@setProses')->name('setprosesvocal');

// kalkulator
Route::get('/kalkulator', 'KalkulatorController@index');


