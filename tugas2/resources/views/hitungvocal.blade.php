@extends('template')
 
@section('content')
<br>

<form action="{{route('setprosesvocal')}}" method="POST" class=" mt-2 ml-3">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Example textarea</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" style="width: 300px" name="name"></textarea>
      </div> 
    <button type="submit" class="btn btn-primary">Simpan</button>

    </form>
    <div>
        @if (!empty($result))
        <p>
            "{{$name}}" = {{count($result)}} yaitu
            @php
                $i = 0;
                $length = count($result);
            @endphp
            @foreach ($result as $index => $item)
                <span>{{$item}}</span>
                @php
                   echo ($i == $length - 1) ? "<br>" : " dan ";
                    $i++;
                @endphp
            @endforeach
        </p>
        @endif
        
    </div>
</div>



@endsection