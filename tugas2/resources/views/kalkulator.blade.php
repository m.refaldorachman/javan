@extends('template')

@section('content')
<form action="" method="" class="mt-4 ml-5">
@csrf
    <div class="form-group ">
        <label for="inputOne">Input Pertama</label>
        <input type="text" name="valueOne" id="inputOne" value="{{$inputOne}}">
    </div>
    <div class="form-group">
        <label for="inputTwo">Input Kedua</label>
        <input type="text" name="valueTwo" id="inputTwo" value="{{$inputTwo}}">
    </div> <div class="form-group">
        <label for="result">Hasil</label>
        <input type="text" name="result" id="result" value="{{$result}}" >
    </div>
    <div class="wrapper-button">
        <button type="submit" name="operation" class="btn btn-success" value="+">+</button>
        <button type="submit" name="operation" class="btn btn-success" value="-">-</button>
        <button type="submit" name="operation" class="btn btn-success" value="*">*</button>
        <button type="submit" name="operation" class="btn btn-success" value="/">/</button>
    </div>
</form>

@endsection