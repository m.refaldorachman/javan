@extends('template')
 
@section('content')
<br>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{route('setproses')}}" method="POST" class=" mt-2">
    @csrf
    <input type="number" placeholder="Awal" name="awal"   />
    <input type="number" placeholder="Akhir" name="akhir"  />
    <button type="submit" class="btn btn-primary">Simpan</button>
    </form>


    @if (!empty($data))
        @foreach($data as $row)
            @php
            echo $row 
            @endphp
        @endforeach
    @endif


@endsection