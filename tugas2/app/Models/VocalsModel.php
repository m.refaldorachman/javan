<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VocalsModel extends Model
{
    public static function setCountVocal($value)
    {
        $vocals = ['a', 'i', 'u', 'e', 'o'];
        $countVocal = array_intersect($vocals, str_split($value));
        return $countVocal;
    }
}
