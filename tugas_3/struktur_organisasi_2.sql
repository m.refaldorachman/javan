-- Insert data table to employee
INSERT INTO employee(`nama`, `atasan_id`, `company_id`) VALUES
	('Pak Budi', null, '1'),   
	('Pak Tono', '1', '1'),   
	('Pak Totok', '1', '1'),   
	('Bu Sinta', '2', '1'),   
	('Bu Novi', '3', '1'),   
	('Andre', '4', '1'),   
	('Dono', '4', '1'),
	('Ismair', '5', '1'),
	('Anto', '5', '1');

-- Insert data table to company
INSERT INTO company(`nama`, `alamat`) VALUES
	('PT JAVAN', 'Sleman'),   
	('PT Dicoding', 'Bandung');
-- End Insert Table

-- Ceo
SELECT * FROM employee where atasan_id is null;
-- Staff biasa
SELECT * FROM employee where atasan_id >= 4;
-- Menampilkan orang yang dibawah Direktur
SELECT * FROM employee where atasan_id = 1;
-- Menampilan orang yang dibawah Manajer
SELECT * FROM employee where atasan_id = 4 or atasan_id = 5;


-- Menampilkan jumlah karyawan bawahan pak budi
SELECT count(*) as Karyawan_Pak_Budi FROM employee where employee.nama != "Pak Tono";
-- Menampilkan jumlah karyawan bawahan  bu shinta
SELECT  count(*) as Bawahan_Bu_Shinta FROM employee WHERE employee.nama != "Bu Sinta" and employee.atasan_id = '4';
